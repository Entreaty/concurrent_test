package com.example;

public abstract class Supplier {
	public int maxConcurrentOps;

	public Supplier(int maxConcurrentOps) {
		this.maxConcurrentOps = maxConcurrentOps;
	}

	/**
	 * Asks supplier to set up a stock of products for provided partNumber.
	 * Method returns immediately and doesn't wait until operation completes.
	 *
	 * @param partNumber
	 * @param amountToStock
	 */
	abstract void stock(int partNumber, int amountToStock);

	/**
	 * Ask supplier to terminate stock of products with this partNumber.
	 * Method returns immediately and doesn't wait until operation completes.
	 *
	 * @param partNumber
	 */
	abstract void destock(int partNumber);

	/**
	 * If the requested partNumber was stocked, method returns an instance of a Resource
	 * from the stock. If the requested partNumber was not stocked, method produces it
	 * and returns to caller.
	 *
	 * @param partNumber
	 * @return
	 */
	abstract Resource request(int partNumber);
}