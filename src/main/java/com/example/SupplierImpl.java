package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class SupplierImpl extends Supplier {


	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);
	private ExecutorManager executorManager;
	// Stock as a ConcurrentHashMap
	private ConcurrentHashMap<Integer, LinkedList<Future<Resource>>> concurrentHashMap = new ConcurrentHashMap<>();

	SupplierImpl(int maxConcurrentOps) {
		super(maxConcurrentOps);
		executorManager = ExecutorManager.getInstance();
		executorManager.start(maxConcurrentOps);
	}

	@Override
	synchronized void stock(int partNumber, int amountToStock) {
		log.info("Stock starting");
		manageStock(partNumber, amountToStock);
	}

	@Override
	synchronized void destock(int partNumber) {
		log.info("Destock starting");
		try {
			if (!executorManager.executor.awaitTermination(1000, TimeUnit.MILLISECONDS)) {
				if (concurrentHashMap.containsKey(partNumber)) {
					concurrentHashMap.remove(partNumber);
					executorManager.restart(maxConcurrentOps);
				}
				log.info("ConcurrentHashMap = {}", concurrentHashMap.toString());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@Override
	synchronized Resource request(int partNumber) {
		log.info("ConcurrentHashMap before requesting= {}", concurrentHashMap.toString());
		try {
			log.info("Thread {} Request starting for {}", Thread.currentThread().getName(), partNumber);
			if (!concurrentHashMap.containsKey(partNumber)) {
				Callable<Resource> callableTask = () -> Resource.make(partNumber);
				Resource resource = executorManager.executor.submit(callableTask).get();
				log.info("Thread {} Your resource from new instance for partNumber {} = {}",
						Thread.currentThread().getName(), partNumber, resource);
				return resource;
			} else {
				Future<Resource> resource = concurrentHashMap.get(partNumber).poll();
				log.info("Thread {} Your resource from stock for partNumber {} = {}",
						Thread.currentThread().getName(), partNumber, resource);
				LinkedList<Future<Resource>> list = concurrentHashMap.get(partNumber);
				fillStock(partNumber, list, 1);
				return resource.get();
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	synchronized void dismiss(Resource resource) {
		if (resource != null) {
			executorManager.executor.execute(() -> resource.dismiss());
		}
	}

	/**
	 * Tries to find list for current partNumber in CHMap
	 * and decides to increase or decrease it
	 */
	private void manageStock(int partNumber, int amountToStock) {
		log.info("ConcurrentHashMap before managing stock = {}", concurrentHashMap.toString());
		LinkedList<Future<Resource>> list;
		if (concurrentHashMap.get(partNumber) != null) {
			list = concurrentHashMap.get(partNumber);
		} else {
			list = new LinkedList<>();
		}
		int shortage = amountToStock - list.size();
		if (shortage < 0) {
			destock(partNumber);
			fillStock(partNumber, list, amountToStock);
		} else if (shortage > 0) {
			fillStock(partNumber, list, shortage);
		}
	}

	/**
	 * Added object Future<Resource> into stock(concurrentHashMap)
	 * with key partNumber
	 */
	private void fillStock(int partNumber, LinkedList<Future<Resource>> list, int amountToAdd) {
		for (int i = 0; i < amountToAdd; i++) {
			Callable<Resource> callableTask = () -> Resource.make(partNumber);
			list.add(executorManager.executor.submit(callableTask));
			concurrentHashMap.put(partNumber, list);
			log.info("Resource added to stock for {}. Size of list = {}", partNumber, list.size());
			executorManager.executor.submit(callableTask);
		}
	}
}