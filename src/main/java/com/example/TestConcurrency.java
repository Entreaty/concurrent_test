package com.example;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestConcurrency {
	public static void main(String[] args) {
		Executor executor = Executors.newFixedThreadPool(3);
		SupplierImpl supplier = new SupplierImpl(2);
		Runnable runnableTask1 = () -> {
			supplier.destock(5);
			supplier.stock(2, 5);
			supplier.stock(5, 5);
			supplier.destock(0);
			supplier.stock(0, 5);
		};
		executor.execute(runnableTask1);

		Runnable runnableTask2 = () -> {
			try {
				TimeUnit.SECONDS.sleep(6);
				supplier.request(2);
				supplier.request(2);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		executor.execute(runnableTask2);

		Runnable runnableTask3 = () -> {
			try {
				TimeUnit.SECONDS.sleep(6);
				supplier.request(2);
				supplier.request(2);
				supplier.request(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		executor.execute(runnableTask3);

	}
}
