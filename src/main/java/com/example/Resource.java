package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Resource {
	private final int partNumber;
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);


	public Resource(int partNumber) {
		this.partNumber = partNumber;
	}

	public static Resource make(int partNumber) {
		try {
			int timeToSleep = partNumber * 1000;
			Thread.sleep(timeToSleep);
			return new Resource(partNumber);
		} catch (InterruptedException e) {
			return null;
		}
	}

	public void dismiss() {
		try {
			int timeToSleep = partNumber * 1000;
			Thread.sleep(timeToSleep);
		} catch (InterruptedException e) {
		}
	}
}
