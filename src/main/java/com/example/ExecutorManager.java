package com.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorManager {
	private static final ExecutorManager INSTANCE = new ExecutorManager();

	private ExecutorManager() {
	}

	public ExecutorService executor;

	public static ExecutorManager getInstance() {
		return INSTANCE;
	}

	public void start(int maxThreads) {
		executor = Executors.newFixedThreadPool(maxThreads);
	}

	public void restart(int maxThreads) {
		executor.shutdownNow();
		executor = Executors.newFixedThreadPool(maxThreads);
	}
}
