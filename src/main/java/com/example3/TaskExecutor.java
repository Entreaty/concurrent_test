package com.example3;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskExecutor implements Runnable {
	private BlockingQueue queue;
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);

	public TaskExecutor(BlockingQueue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		try {
			while (true) {
				String name = Thread.currentThread().getName();
				Runnable task = queue.dequeue();
				log.info("Task Started by Thread {}", name);
				task.run();
				log.info("Task Finished by Thread {}", name);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}