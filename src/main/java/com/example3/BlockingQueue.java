package com.example3;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

public class BlockingQueue {
	private Queue<TestTask> queue = new LinkedList<TestTask>();
	private int EMPTY = 0;
	private int MAX_TASK_IN_QUEUE = -1;
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);

	public BlockingQueue(int size) {
		this.MAX_TASK_IN_QUEUE = size;
	}

	public synchronized void enqueue(TestTask task)
			throws InterruptedException {
		while (this.queue.size() == this.MAX_TASK_IN_QUEUE) {
			wait();
		}
		if (this.queue.size() == EMPTY) {
			notifyAll();
		}
		this.queue.offer(task);
		log.info("Task added {}", task);
	}

	public synchronized TestTask dequeue()
			throws InterruptedException {
		while (this.queue.size() == EMPTY) {
			wait();
		}
		if (this.queue.size() == this.MAX_TASK_IN_QUEUE) {
			notifyAll();
		}
		log.info("Task removed");
		return this.queue.poll();
	}
}