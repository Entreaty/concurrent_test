package com.example3;

public class TestThreadPool {
	public static void main(String[] args) throws InterruptedException {
		ThreadPool threadPool = new ThreadPool(3, 5);
		for (int taskNumber = 1; taskNumber <= 6; taskNumber++) {
			TestTask task = new TestTask(taskNumber);
			threadPool.submitTask(task);
		}
	}
}