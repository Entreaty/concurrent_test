package com.example3;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestTask implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);
	private int number;

	public TestTask(int number) {
		this.number = number;
	}

	@Override
	public void run() {
		log.info("Start executing of task number {}", number);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("End executing of task number {}", number);
	}
}