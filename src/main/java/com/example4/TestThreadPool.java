package com.example4;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class TestThreadPool {
	public static void main(String[] args) throws InterruptedException {
		ConcurrentHashMap<Integer, LinkedList<Resource>> resourceMap = new ConcurrentHashMap<>();
		ThreadPool threadPool = new ThreadPool(3, 5);

		for (int taskNumber = 1; taskNumber <= 6; taskNumber++) {
			TestTask task = new TestTask(5, resourceMap);
			threadPool.submitTask(task);
		}
	}
}