package com.example4;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

public class SupplierImpl extends Supplier {

	private final LinkedList<Integer> tasks = new LinkedList<>();
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);
	private ConcurrentHashMap<Integer, LinkedList<Future<Resource>>> resources = new ConcurrentHashMap<>();

	public SupplierImpl(int maxConcurrentOps) {
		super(maxConcurrentOps);
	}

	@Override
	synchronized void stock(int partNumber, int amountToStock) {
	}

	@Override
	synchronized void destock(int partNumber) {

	}

	@Override
	Resource request(int partNumber) {
		return null;
	}
}
