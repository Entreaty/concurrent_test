package com.example4;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class TestTask implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);
	private int partNumber;
	private ConcurrentHashMap<Integer, LinkedList<Resource>> concurrentHashMap;

	public TestTask(int partNumber, ConcurrentHashMap<Integer, LinkedList<Resource>> concurrentHashMap) {
		this.partNumber = partNumber;
		this.concurrentHashMap = concurrentHashMap;
	}

	@Override
	public void run() {
		log.info("Start executing of task partNumber {}", partNumber);
		LinkedList<Resource> list;
		if(concurrentHashMap.containsKey(partNumber)){
			list = concurrentHashMap.get(partNumber);
		}else
		{
			list = new LinkedList<>();
		}
		list.add(Resource.make(3));
		concurrentHashMap.put(partNumber, list);
		log.info("Map = {}", concurrentHashMap);
		log.info("End executing of task partNumber {}", partNumber);
	}
}