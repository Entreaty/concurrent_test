package com.example2;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Resource {
	private final int partNumber;
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);


	public Resource(int partNumber) {
		this.partNumber = partNumber;
	}

	public static Resource make(int partNumber) {
		try {
			log.info("Resource start creating for partNumber {}", partNumber);
			int timeToSleep = partNumber * 1000;
			Thread.sleep(timeToSleep);
			log.info("Resource finish creating for partNumber {}, thread = {}", partNumber, Thread.currentThread()
					.getName());
			return new Resource(partNumber);
		} catch (InterruptedException e) {
			return null;
		}
	}

	public void dismiss() {
		try {
			log.info("Resource start dismissing for partNumber {}", partNumber);
			int timeToSleep = partNumber * 1000;
			Thread.sleep(timeToSleep);
			log.info("Resource finish dismissing for partNumber {}", partNumber);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
