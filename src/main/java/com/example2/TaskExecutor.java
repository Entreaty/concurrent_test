package com.example2;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

class TaskExecutor implements Runnable {
	private final LinkedList<Integer> taskQueue;
	private final ConcurrentHashMap<Integer, LinkedList<Resource>> resources;

	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);

	public TaskExecutor(LinkedList<Integer> tasks, ConcurrentHashMap<Integer, LinkedList<Resource>> resources) {
		this.taskQueue = tasks;
		this.resources = resources;
	}

	@Override
	public void run() {
		try {
			while (true) {
				int partNumber = getTask();
				if (partNumber < 0) {
					partNumber = Math.abs(partNumber);
					for (int i = 0; i < resources.get(partNumber).size(); i++) {
						Resource resource = resources.get(partNumber).poll();
						if (resource != null) resource.dismiss();
					}
				} else {
					String oldName = Thread.currentThread().getName();
					Thread.currentThread().setName(oldName+"_"+partNumber);
					resources.get(partNumber).add(Resource.make(partNumber));
					Thread.currentThread().setName(oldName);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private int getTask() throws InterruptedException {
		synchronized (taskQueue) {
			while (taskQueue.isEmpty()) {
				log.info("Queue is empty {} is waiting", Thread.currentThread().getName());
				taskQueue.wait();
			}
			int result = taskQueue.poll();
			log.info("{} got the task with partNumber{}", Thread.currentThread().getName(), result);
			return result;
		}
	}
}