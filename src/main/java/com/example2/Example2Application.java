package com.example2;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Example2Application {
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);

	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		SupplierImpl supplier = new SupplierImpl(3);

		Runnable runnableTask1 = () -> {
			try {
				TimeUnit.SECONDS.sleep(0);
				supplier.stock(2, 9);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		executor.execute(runnableTask1);

		Runnable runnableTask2 = () -> {
			try {
				TimeUnit.SECONDS.sleep(1);
				supplier.request(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		executor.execute(runnableTask2);
//		executor.shutdown();
//
		Runnable runnableTask3 = () -> {
			try {
				TimeUnit.SECONDS.sleep(2);
				supplier.stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		executor.execute(runnableTask3);
		executor.shutdown();
//
//		Runnable runnableTask4 = () -> {
//			try {
//				TimeUnit.SECONDS.sleep(7);
//				supplier.destock(5);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		};
//		executor.execute(runnableTask4);
//
//		Runnable runnableTask5 = () -> {
//			try {
//				TimeUnit.SECONDS.sleep(20);
//				supplier.request(2);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		};
//		executor.execute(runnableTask5);
//		Runnable runnableTask6 = () -> {
//			try {
//				TimeUnit.SECONDS.sleep(40);
//				supplier.destock(5);
//				supplier.request(2);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		};
//		executor.execute(runnableTask6);
//		Runnable runnableTask7 = () -> {
//			try {
//				TimeUnit.SECONDS.sleep(5);
//				supplier.stop();
//				executor.shutdownNow();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		};
//
//		executor.execute(runnableTask7);

	}
}
