package com.example2;

import com.example.TestConcurrency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class SupplierImpl extends Supplier {

	private final LinkedList<Integer> tasks = new LinkedList<>();
	private final LinkedList<Thread> threads = new LinkedList<>();
	private static final Logger log = LoggerFactory.getLogger(TestConcurrency.class);
	private ConcurrentHashMap<Integer, LinkedList<Resource>> resources = new ConcurrentHashMap<>();

	public SupplierImpl(int maxConcurrentOps) {
		super(maxConcurrentOps);
		for (int i = 0; i < maxConcurrentOps; i++) {
			Thread thread = (new Thread(new TaskExecutor(tasks, resources), "TaskExecutor" + i));
			threads.add(thread);
			thread.start();
		}
	}

	@PreDestroy
	public void stop() {
		log.info("Interrupt");
		while (!threads.isEmpty()) {
			threads.poll().interrupt(); // threads.poll().stop() possibly this way will be better
			log.info("{} is interrupted", Thread.currentThread().getName());
		}
	}

	@Override
	synchronized void stock(int partNumber, int amountToStock) {
		if (!resources.containsKey(partNumber)) {
			resources.put(partNumber, new LinkedList<>());
		}
		LinkedList<Resource> list = resources.get(partNumber);
		int shortage = amountToStock - list.size();
		if (shortage > 0) {
			createTasks(partNumber, amountToStock);
		} else if (shortage < 0) {
			for (int i = 0; i < shortage; i++) {
				log.info("New amount is less. Count of resources for destocking = {}", shortage);
				createTasks(-partNumber, shortage);
			}
		}
	}

	@Override
	synchronized void destock(int partNumber) {
		if (resources.containsKey(partNumber)) {
			// Clear taskQueue for current partNumber
			for (int i = 0; i < tasks.size(); i++) {
				if (tasks.get(i) == partNumber) {
					tasks.remove(i);
				}
			}
			// Cancel all processes of creating new Resources at the moment
			restartThreadsForPartNumber(partNumber);
			// Create tasks for dismissing created resources
			int size = resources.get(partNumber).size();
			log.info("Count of resources for destocking = {}", size);
			createTasks(-partNumber, size);
		}
	}

	private void createTasks(int partNumber, int amount) {
		for (int i = 0; i < amount; i++) {
			if (partNumber < 0) {
				tasks.addFirst(partNumber);
			} else {
				tasks.add(partNumber);
			}
		}
		synchronized (tasks) {
			tasks.notifyAll();
		}
	}

	@Override
	Resource request(int partNumber) {
		if (resources.containsKey(partNumber) && !resources.get(partNumber).isEmpty()) {
			Resource resource = resources.get(partNumber).poll();
			createTasks(partNumber, 1);
			return resource;
		} else {
			return Resource.make(partNumber);
		}
	}

	public void getStatus() {
		log.info("Resources {} Tasks {}", resources, tasks);
	}

	/**
	 * Restart threads which creates Resources for current partNumber at the moment
	 */
	private void restartThreadsForPartNumber(int partNumber) {
		int deadThread = 0;
		for (int i = 0; i < threads.size(); i++) {

			String threadName = threads.get(i).getName();
			String[] chr = threadName.split("_");
			if (chr.length > 1) {
				int threadPartNumber = Integer.parseInt(chr[1]);
				if (threadPartNumber == partNumber) {
					threads.get(i).interrupt();
					threads.remove(threads.get(i));
					deadThread++;
				}
			}
			log.info("deadThread = {}", deadThread);
		}
		for (int i = 0; i < deadThread; i++) {
			Thread thread = (new Thread(new TaskExecutor(tasks, resources), "TaskExecutor" + i));
			threads.add(thread);
			thread.start();
		}
	}
}